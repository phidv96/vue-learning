var vueInstance = new Vue({
    el: '#app',
    data() {
        return {
            awesome: true,
            tabSelected: 'login',
        }
    },
    methods: {
        toggle() {
            this.awesome = !this.awesome
        },
        changeTabs(tab) {
            this.tabSelected = tab
        }
    },
});