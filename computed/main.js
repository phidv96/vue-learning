let id = 0

var vueInstance = new Vue({
    el: '#app',
    data() {
        return {
            a: 0,
            b: 0,
            number: 100
        }
    },
    methods: {
    },
    computed: {
        addA() {
            return this.a + this.number
        },
        addB() {
            return this.b + this.number
        },
    }
});