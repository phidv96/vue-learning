var vueInstance = new Vue({
    el: '#app',
    data() {
        return {
            title: 'learning',
            count: 0,
            clientX: 0,
            clientY: 0
        }
    },
    methods: {
        increment() {
            this.count++
        },
        handleMousemove(e){
            this.clientX = e.clientX;
            this.clientY = e.clientY;
        }
    },
});