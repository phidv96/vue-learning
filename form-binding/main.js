var vueInstance = new Vue({
    el: '#app',
    data() {
        return {
            text: ''
        }
    },
    methods: {
        onInput(e) {
            this.text = e.target.value
          }
    },
});