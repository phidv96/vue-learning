let id = 0

var vueInstance = new Vue({
    el: '#app',
    data() {
        return {
            title: "title",
            a: 0,
            number: 100
        }
    },
    methods: {
    },
    computed: {
        addA() {
            return this.a + this.number
        },
    },
    beforeCreate() {
        console.log("beforeCreate", this.title, document.querySelector('.container'))    
    },
    created() {
        // call api, ajax
        console.log("created", this.title, document.querySelector('.container'))  
    },
    beforeMount() {
        console.log("beforeMount", this.title, document.querySelector('.container'))  
    },
    mounted() {
        // can use jQuery, Js
        console.log("mounted", this.title, document.querySelector('.container'))  
    },
    beforeUpdate() {
        //run when change data
        console.log("beforeUpdate")  
    },
    updated() {
        //run when change data
        console.log("updated")  
    },
    beforeDestroy() {
        console.log("beforeDestroy")  
    },
    destroyed() {
        // destroy library
        console.log("destroyed")  
    },
});